# numix-cinnamon-theme-packaging

This thing creates packages for [numix cinnamon theme](https://github.com/zagortenay333/numix-cinnamon) using [fpm](https://github.com/jordansissel/fpm)

Installing through Spices is cool for a single user, but i want to distribute this excellent theme for a wider setup and set as default (using [dconf](https://gitlab.com/packaging/dconf-branding)) for all users. Therefore i need a package for salt/puppet/whateveryoulike.
